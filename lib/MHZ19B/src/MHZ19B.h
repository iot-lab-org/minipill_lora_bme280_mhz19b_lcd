/*
  MHZ19B.h - Library for MH-Z19B Serial Co2 sensor
  Created by Leo Korbee, 2017-09-19.
  Not released into the public domain.
*/

#ifndef MHZ19B_h
#define MHZ19B_h

#define CO2SERIAL Serial2

#include "Arduino.h"

class MHZ19B
{
  public:
    /*
     * A simplified constructor taking only a Stream ({Software/Hardware}Serial) object.
     * The serial port should already be initialised when initialising this library.
     */
    MHZ19B(Stream& serial);
    void begin();
    int getCo2();

  private:
    Stream& _serial;


};

#endif
