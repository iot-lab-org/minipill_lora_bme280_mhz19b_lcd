/*
  main.cpp
  Main code where the control takes place
  @author  Leo Korbee (c), Leo.Korbee@xs4all.nl
  @website iot-lab.org
  @license Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
  Thanks to all the folks who contributed beforme me on this code or libraries.
  Please share you thoughts and comments or you projects with me through e-mail
  or comment on my website

  Hardware information at the end of this file.

  @version 2020-07-12
  Added LMIC_setClockError due to inaccurate timing on LMIC

  @version 2020-09-20
  Added Co2 sensor

  @version 2020-09-20a
  Added BME280 and RFM95W for LoRa capabilities

  @version 2020-09-23
  Show more data on LCD screen
  Remove Low power lib and usage

*/

#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include "STM32IntRef.h"
#include "BME280.h"
#include "MHZ19B.h"
#include "U8g2lib.h" // LCD display

void do_send(osjob_t* j);

/* A BME280 object using SPI, chip select pin PA1 */
BME280 bme(SPI,PA1);

// connect to MH-Z19B sensor to the hardware Serial2
HardwareSerial Serial2(USART2);
MHZ19B mhzSensor(Serial2);

// init LCD display
U8G2_ST7567_JLX12864_F_4W_HW_SPI u8g2(U8G2_R0, PB10, PB8, PB9);

// include security credentials OTAA, check secconfig_example.h for more information
#include "secconfig.h"
void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}
void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}
void os_getDevKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}

static osjob_t sendjob;

// Hold this many seconds. Notice that the sending and waiting for downlink
// will extend the time between send packets. You have to extract this time
const unsigned TX_INTERVAL = 300;

// count number of transmits
int txCounter = 0;

// Pin mapping for the MiniPill LoRa with the RFM95 LoRa chip
const lmic_pinmap lmic_pins =
{
  .nss = PA4,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = PA9,
  .dio = {PA10, PB4, PB5},
};

// event called by os on events
void onEvent (ev_t ev)
{
     switch(ev) {
         case EV_SCAN_TIMEOUT:
             break;
         case EV_BEACON_FOUND:
             break;
         case EV_BEACON_MISSED:
             break;
         case EV_BEACON_TRACKED:
             break;
         case EV_JOINING:
             break;
         case EV_JOINED:
             // Disable link check validation (automatically enabled
             // during join, but not supported by TTN at this time).
             LMIC_setLinkCheckMode(0);
             break;
         case EV_RFU1:
             break;
         case EV_JOIN_FAILED:
             break;
         case EV_REJOIN_FAILED:
             break;
             break;
         case EV_TXCOMPLETE:
             if (LMIC.txrxFlags & TXRX_ACK)
             if (LMIC.dataLen)
             {
               // print data
               for (int i = 0; i < LMIC.dataLen; i++)
               {
               }
             }
             delay(100);
             // set PA6 to analog to reduce power due to currect flow on DIO on BME280
             pinMode(PA6, INPUT_ANALOG);

            // next transmission
             os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
             break;
         case EV_LOST_TSYNC:
             break;
         case EV_RESET:
              break;
         case EV_RXCOMPLETE:
             // data received in ping slot
             break;
         case EV_LINK_DEAD:
             break;
         case EV_LINK_ALIVE:
             break;
          default:
             break;
     }
}

void do_send(osjob_t* j)
{
  // Check if there is not a current TX/RX job running
  if (LMIC.opmode & OP_TXRXPEND)
  {
  } else
  {
    // increase txCounter
    txCounter++;

    // Prepare upstream data transmission at the next possible time.
    uint8_t dataLength = 10;
    uint8_t data[dataLength];

    // read vcc and add to bytebuffer
    int32_t vcc = IntRef.readVref();

    // reading data from BME sensor
    bme.readSensor();
    float tempFloat = bme.getTemperature_C();
    float humFloat = bme.getHumidity_RH();
    float pressFloat = bme.getPressure_Pa();

    // from float to uint16_t
    uint16_t tempInt = 100 * tempFloat;
    uint16_t humInt = 100 * humFloat;
    // pressure is already given in 100 x mBar = hPa
    uint16_t pressInt = pressFloat/10;

    // get data from co2sensor
    uint16_t co2 = mhzSensor.getCo2();

    // generic char buffer for conversion
    char buf[10];

    u8g2.firstPage();
    u8g2.clearDisplay();
    do {
      // u8g2.setFont(u8g_font_unifont);
      u8g2.setFont(u8g_font_helvB08);

      u8g2.drawStr(0,10, "Co2:");
      itoa(co2, buf, 10);
      strcat(buf, " ppm");
      u8g2.drawStr(40,10, buf);

      u8g2.drawStr(0,21, "Temp:");
      dtostrf(tempFloat, 5, 1, buf);
      strcat(buf, " \xB0""C");
      u8g2.drawStr(40,21, buf);

      u8g2.drawStr(0,32, "Hum:");
      dtostrf(humFloat, 5, 1, buf);
      strcat(buf, " %");
      u8g2.drawStr(40,32, buf);

      u8g2.drawStr(0,43, "Pres:");
      dtostrf(pressFloat/100, 5, 1, buf);
      strcat(buf, " mBar");
      u8g2.drawStr(40,43, buf);

      u8g2.drawStr(0,54, "Count:");
      itoa(txCounter, buf, 10);
      strcat(buf, " txEvents");
      u8g2.drawStr(40,54, buf);
    } while( u8g2.nextPage());


    // move data into databuffer
    data[0] = (vcc >> 8) & 0xff;
    data[1] = (vcc & 0xff);

    data[2] = (tempInt >> 8) & 0xff;
    data[3] = tempInt & 0xff;

    data[4] = (humInt >> 8) & 0xff;
    data[5] = humInt & 0xff;

    data[6] = (pressInt >> 8) & 0xff;
    data[7] = pressInt & 0xff;

    data[8] = (co2 >> 8) & 0xff;
    data[9] = co2 & 0xff;

    // set forced mode to be shure it will use minimal power and send it to sleep
    bme.setForcedMode();
    bme.goToSleep();
    // set data in queue
    LMIC_setTxData2(1, data, sizeof(data), 0);
  }
}

void setup()
{
  // delay at startup for debugging reasons
  delay(1000);
  // LMIC init
  os_init();
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();
  // to incrise the size of the RX window.
  LMIC_setClockError(MAX_CLOCK_ERROR * 10 / 100);

  // begin communication with BME280 and set to default sampling, iirc, and standby settings
  if (bme.begin() < 0)
  {
    while(1){}
  }

  // begin Co2 sensor
  mhzSensor.begin();

  // LCD display
  u8g2.begin();
  u8g2.setContrast(100);

  u8g2.firstPage();
  u8g2.clearDisplay();
  do {
    // u8g2.setFont(u8g_font_unifont);
    u8g2.setFont(u8g_font_helvB10);
    u8g2.drawStr(10,15, "www.iot-lab.org");
    u8g2.setFont(u8g_font_helvB08);
    u8g2.drawStr(5, 30, "Environment LoRa node");
    u8g2.drawStr(5, 52, "trying to join TTN...");
  } while( u8g2.nextPage());

  delay(5000);
  // Start job (sending automatically starts OTAA too)
  do_send(&sendjob);
}

void loop()
{
  // run the os_loop to check if a job is available
  os_runloop_once();
}


/* MiniPill LoRa v1.1 mapping - LoRa module RFM95W and BME280 sensor
  PA1  //            NSS           - BME280
  PA4  // SPI1_NSS   NSS  - RFM95W
  PA5  // SPI1_SCK   SCK  - RFM95W - BME280
  PA6  // SPI1_MISO  MISO - RFM95W - BME280
  PA7  // SPI1_MOSI  MOSI - RFM95W - BME280

  PA10 // USART1_RX  DIO0 - RFM95W
  PB4  //            DIO1 - RFM95W
  PB5  //            DIO2 - RFM95W

  PA9  // USART1_TX  RST  - RFM95W

  VCC - 3V3
  GND - GND
*/
