/*
  MHZ19B.cpp - Library for MH-Z19B Serial Co2 sensor
  Created by Leo Korbee, 2017-09-19.
  Not released into the public domain.
*/



#include <Arduino.h>
#include <MHZ19B.h>


// constructor
MHZ19B::MHZ19B(Stream& serial):
_serial(serial)
{
  //initiate Software serial with two pins
  //SoftwareSerial co2Serial(rx, tx);
}

// init method
void MHZ19B::begin()
{
  //co2Serial.begin(9600); //Init sensor MH-Z19(14)
  CO2SERIAL.begin(9600);
}


// method to get Co2
int MHZ19B::getCo2()
{


  byte cmd[9] = {0xFF, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79};
  // command to ask for data
  byte response[9]; // for answer

  CO2SERIAL.write(cmd, 9); //request PPM CO2

  // The serial stream can get out of sync. The response starts with 0xff, try to resync.
  while (CO2SERIAL.available() > 0 && (unsigned char)CO2SERIAL.peek() != 0xFF) {
    CO2SERIAL.read();
  }

  memset(response, 0, 9);
  CO2SERIAL.readBytes(response, 9);

  // debugging response
  /*
  Serial.print("output MHZ19B sensor: ");
  for (int i = 0; i < 9; i++)
  {
    Serial.print(response[i], HEX);
    Serial.print(" ");
  }
  Serial.println();
  */

  if (response[1] != 0x86)
  {
    Serial.println("Invalid response from co2 sensor!");
    return -1;
  }

// decode response:
//
//  co2 = buf[2] * 256 + buf[3];
//  		co2temp = buf[4] - 40;
//  		co2status =  buf[5]; is warming?

// https://revspace.nl/MHZ19 for configuration!


  byte crc = 0;

  for (int i = 1; i < 8; i++)
  {
    crc += response[i];
  }

  crc = 255 - crc + 1;

  if (response[8] == crc)
  {
    int responseHigh = (int) response[2];
    int responseLow = (int) response[3];
    int ppm = (256 * responseHigh) + responseLow;
    return ppm;
  }
  else
  {
    Serial.println("CRC error!");
    return -1;
  }

}
